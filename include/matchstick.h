/*
** EPITECH PROJECT, 2020
** CPE_matchstick_2019
** File description:
** matchstick
*/

#ifndef MATCHSTICK_H_
#define MATCHSTICK_H_

#include "struct.h"

// matchstick
void create_game(game_t *game, int line, int m_matches);
int render_game(game_t *game);
void destroy_game(game_t *game);

// map
void center_character(int nb_base, int size_line, map_t *map, int x);
void fill_map(map_t *map);
void alloc_map(map_t *map);
void print_map(map_t map);
map_t create_map(int line_count);

// matches
int count_all_line(map_t map);
int count_matches_line(map_t map, int line);
void remove_matches(map_t *map, int line, int to_remove);

// player_manager
int get_p_line(game_t *game);
int get_p_matche(game_t *game, int line);
int player_turn(game_t *game);

// ia_manager
int ia_turn(game_t *game);

// error_man
void print_error(game_t *game, int nbr, int lim);

#endif /* !MATCHSTICK_H_ */