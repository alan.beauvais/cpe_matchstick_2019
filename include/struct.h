/*
** EPITECH PROJECT, 2020
** CPE_matchstick_2019
** File description:
** struct
*/

#ifndef STRUCT_H_
#define STRUCT_H_

typedef struct map
{
    int line_c;
    int line_s;
    char **map;
} map_t;

typedef struct game
{
    int line;
    int n_matches_rm;
    map_t map;
} game_t;

typedef struct turn
{
    char *name;
    int (*function)(game_t * game);
    char *lost;
} turn_t;

#endif /* !STRUCT_H_ */
