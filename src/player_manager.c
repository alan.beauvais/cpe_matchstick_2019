/*
** EPITECH PROJECT, 2020
** CPE_matchstick_2019
** File description:
** player_manager
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "matchstick.h"
#include "printlib.h"
#include "struct.h"

const char *ERROR_INPUT = "Error: invalid input (positive number expected)";
const char *ERROR_LINE = "Error: this line is out of range";
const char *ERROR_MATC = "Error: not enough matches on this line";

int invalid_number(char const *str)
{
    if (my_strlen(str) == 1)
        return 1;
    for (int i = 0; i < my_strlen(str) && str[i] != '\n'; i++) {
        if (str[i] < '1' || str[i] > '9')
            return 1;
    }
    return 0;
}

int get_p_line(game_t *game)
{
    int nbr = 0;
    size_t size = 0;
    char *buffer = NULL;

    my_printf("\nLine: ");
    if (getline(&buffer, &size, stdin) <= 0 || buffer[0] == '\n')
        return -1;
    if (invalid_number(buffer)){
        my_printf("%s", ERROR_INPUT);
        return get_p_line(game);
    }
    nbr = my_getnbr(buffer);
    if (nbr < 1 || nbr > game->map.line_c) {
        my_printf("%s", ERROR_LINE);
        return get_p_line(game);
    }
    return nbr;
}

int get_p_matche(game_t *game, int line)
{
    int mtc_line = count_matches_line(game->map, line);
    int nbr = 0;
    size_t size = 0;
    char *buffer = malloc(sizeof(char) * 10);
    int lim = 0;

    my_printf("Matches: ");
    if (getline(&buffer, &size, stdin) <= 0 || my_strlen(buffer) != 2)
        return -2;
    if (invalid_number(buffer)) {
        my_printf("%s\n", ERROR_INPUT);
        return -1;
    }
    nbr = my_getnbr(buffer);
    lim = game->n_matches_rm > mtc_line ? mtc_line : game->n_matches_rm;
    if (nbr < 1 || nbr > lim) {
        print_error(game, nbr, lim);
        return -1;
    }
    return nbr;
}

int player_turn(game_t *game)
{
    int line;
    int to_rm;

    if ((line = get_p_line(game)) == -1)
        return -1;
    to_rm = get_p_matche(game, line);
    if (to_rm == -1)
        return player_turn(game);
    else if (to_rm == -2)
        return -1;
    my_printf("Player removed %d match(es) from line %d\n", to_rm, line);
    remove_matches(&game->map, line, to_rm);
    return 1;
}