/*
** EPITECH PROJECT, 2020
** CPE_matchstick_2019
** File description:
** matchstick
*/

#include <stdlib.h>
#include "matchstick.h"
#include "struct.h"
#include "printlib.h"

turn_t turns[] = {
    {"AI's  turn...\n", &ia_turn, "AI wins !"},
    {"Your turn:", &player_turn, "Player win!"}
};

void create_game(game_t *game, int line, int m_matches)
{
    game->line = line;
    game->n_matches_rm = m_matches;
    game->map = create_map(game->line);
}

int render_game(game_t *game)
{
    int turn = 1;

    while (count_all_line(game->map) > 0) {
        print_map(game->map);
        my_printf("%s", turns[turn % 2].name);
        if ((turns[turn % 2].function(game)) == -1)
            return 0;
        turn++;
    }
    my_printf("%s\n", turns[turn % 2].lost);
    return 2 - turn % 2;
}

void destroy_game(game_t *game)
{
    free(game->map.map);
}