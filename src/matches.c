/*
** EPITECH PROJECT, 2020
** CPE_matchstick_2019
** File description:
** matches
*/

#include "matchstick.h"
#include "struct.h"

int count_all_line(map_t map)
{
    int count = 0;

    for (int i = 0; i <= map.line_c; i++)
        count += count_matches_line(map, i);
    return count;
}

int count_matches_line(map_t map, int line)
{
    int count = 0;

    for (int i = 1; map.map[line][i] != '*'; i++) {
        if (map.map[line][i] == '|')
            count++;
    }
    return count;
}

void remove_matches(map_t *map, int line, int to_remove)
{
    int i = map->line_s;
    int count = 0;

    for (; map->map[line][i] != '|'; i--);
    for (; map->map[line][i] == '|' && count < to_remove; i--) {
        map->map[line][i] = ' ';
        count++;
    }
}