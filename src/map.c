/*
** EPITECH PROJECT, 2020
** CPE_matchstick_2019
** File description:
** map
*/

#include <stdlib.h>
#include <stdio.h>
#include "struct.h"
#include "printlib.h"

void center_character(int nb_base, int size_line, map_t *map, int x)
{
    int nb_space = (nb_base - size_line) / 2;
    int i = 0;
    int j = 0;

    map->map[x][0] = '*';
    for (; i < nb_space; i++)
        map->map[x][i + 1] = ' ';
    for (; j < size_line; j++)
        map->map[x][j + i + 1] = '|';
    for (int k = 0; k < nb_space; k++)
        map->map[x][i + j + k + 1] = ' ';
    map->map[x][map->line_s + 1] = '*';
}

void fill_map(map_t *map)
{
    int nb_top = 1;
    int height = map->line_c;
    int nb_base = height + (height - 1);
    int width;
    int i = 0;

    for (; i < 1; i++) {
        for (int j = 0; j < map->line_s + 2; j++)
            map->map[i][j] = '*';
    }
    for (; i < height + 1; i++) {
        width = nb_top + (i - 1) * 2;
        center_character(nb_base, width, map, i);
    }
    for (; i < height + 2; i++) {
        for (int j = 0; j < map->line_s + 2; j++)
            map->map[i][j] = '*';
    }
}

void alloc_map(map_t *map)
{
    for (int i = 0; i < map->line_c + 2; i++)
        map->map[i] = malloc(sizeof(char) * map->line_s + 2);
}

void print_map(map_t map)
{
    for (int i = 0; i < map.line_c + 2; i++) {
        for (int j = 0; j < map.line_s + 2; j++)
            my_putchar(map.map[i][j]);
        my_putchar('\n');
    }
    my_putchar('\n');
}

map_t create_map(int line_count)
{
    map_t map;

    map.line_c = line_count;
    map.line_s = line_count + (line_count - 1);
    map.map = malloc(sizeof(char *) * (map.line_c + 2) * map.line_s);
    alloc_map(&map);
    fill_map(&map);
    return map;
}