/*
** EPITECH PROJECT, 2020
** CPE_matchstick_2019
** File description:
** main
*/

#include "matchstick.h"
#include "struct.h"
#include "printlib.h"

int main(int ac, char **av)
{
    int win;
    game_t game;
    int line = 4;
    int m_matches = 5;

    if (ac == 2 && is_flag(av[1], 1))
        return print_helper(av[0]);
    if (ac != 3)
        return 84;
    if ((line = my_getnbr(av[1])) <= 0 || (m_matches = my_getnbr(av[2])) <= 0)
        return 84;
    create_game(&game, line, m_matches);
    win = render_game(&game);
    destroy_game(&game);
    return win;
}