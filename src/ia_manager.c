/*
** EPITECH PROJECT, 2020
** CPE_matchstick_2019
** File description:
** ia_manager
*/

#include "matchstick.h"
#include "printlib.h"
#include "struct.h"

int ia_turn(game_t *game)
{
    int to_rm = 1;
    int i = 1;

    for (; count_matches_line(game->map, i) < to_rm; i++);
    my_printf("AI removed %d match(es) from line %d\n", to_rm, i);
    remove_matches(&game->map, i, to_rm);
    return 1;
}