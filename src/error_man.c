/*
** EPITECH PROJECT, 2020
** CPE_matchstick_2019
** File description:
** error_man
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "matchstick.h"
#include "printlib.h"
#include "struct.h"

void print_error(game_t *game, int nbr, int lim)
{
    int mx = game->n_matches_rm;

    if (nbr == 0) {
        my_printf("Error: you have to remove at least one match");
        return;
    }
    if (nbr > game->n_matches_rm) {
        my_printf("Error: you cannot remove more than %d matches per turn", mx);
        return;
    }
    if (nbr > lim) {
        my_printf("Error: not enough matches on this line");
        return;
    }
}